import sqlite3
from random import randint
import os
import datetime

def database_init(con):
    with con:
        con.execute("""
            CREATE TABLE IF NOT EXISTS table1 (
               id   INTEGER PRIMARY KEY,
               user VARCHAR(20),
               datastr VARCHAR(12)

    );
        """)

    with con:
        con.execute("""
            CREATE TABLE IF NOT EXISTS table2 (
               user VARCHAR(20),
               message VARCHAR(12)

    );
        """)


    '''
    Заполняю базу данных случайными записями людей.
    Для теста возьмем записи за 05 месяц 2023 года.
    17 записей на 300 юзеров.
    Высока вероятность что таких случайных данных будут такие люди какие нужны по заданию
    '''

    cursor = con.cursor()

    for i in range(300*17): 
        user = f"user{i//15}"

        day    = str(randint(1,31)).rjust(2, "0")
        hour   = str(randint(1,24)).rjust(2, "0")
        minute = str(randint(1,60)).rjust(2, "0")
        second = str(randint(1,60)).rjust(2, "0")

        datastr = f"2305{day}{hour}{minute}{second}"

        request = f"""INSERT INTO table1 (id, user, datastr) VALUES ({i}, '{user}', '{datastr}');"""
        cursor.execute(request)
        con.commit()


def datastr_parser(data):
    year   = int(f"20{data[:2]}")
    moth   = int(data[2:4])
    day    = int(data[4:6])

    return (year, moth, day)


def task(con):
    cursor = con.cursor()

    cursor.execute("""SELECT DISTINCT user FROM table1;""")
    users = cursor.fetchall()

    users_writed = 0
    for user in users:

        cursor.execute(f"""SELECT datastr FROM table1 WHERE user = '{user[0]}'""")
        notes = list(set(map(lambda x: int(x[0][:6]), cursor.fetchall()))) # Получаем datastr обрезая часы минуты и секунды и оставляя только уникальные значения
        notes.sort()
        #Теперь по заданию нужно найти людей которые заходили 7 дней подряд

        last_visit = 0
        count = 0
        for note in notes:
            year, moth, day = datastr_parser(str(note))
            visit = int(datetime.datetime(year, moth, day).strftime("%j"))
            if visit - last_visit == 1:
                count += 1
            else:
                count = 0
            last_visit = visit
            
            if count == 6:
                request = f"""INSERT INTO table2 (user, message) VALUES ('{user[0]}', 'есть запись');"""
                cursor.execute(request)
                users_writed += 1
                con.commit()
                break

    print(f"Количество юзеров с записями - {users_writed}")
def main(path):
    if os.path.isfile(path):
        os.remove(path)

    con = sqlite3.connect(path)

    database_init(con) # Создание базы данных со случайными данными
    
    task(con)


if __name__ == "__main__":
    path = "database.db"
    main(path)
